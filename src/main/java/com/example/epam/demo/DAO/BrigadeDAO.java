package com.example.epam.demo.DAO;

import com.example.epam.demo.DTO.Brigade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class BrigadeDAO implements IBrigadeDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private RowMapper<Brigade> MyRowMapper = new BeanPropertyRowMapper<>(Brigade.class);

    @Override
    public Brigade addBrigade(Brigade brigade) {
        String sql = "INSERT INTO brigade (brigadeName) VALUE (?)";
        jdbcTemplate.update(sql, brigade.getBrigadeName());
        return brigade;
    }

    @Override
    public Brigade getBrigadeById(long brigadeId) {
        String sql = "SELECT * FROM brigade WHERE brigadeId = ?";
        return jdbcTemplate.queryForObject(sql, MyRowMapper, brigadeId);
    }

    @Override
    public void deketeBrigade(long brigadeId) {
        String sql = "DELETE * FROM brigade WHERE brigadeId = ?";
        jdbcTemplate.queryForObject(sql, MyRowMapper, brigadeId);
    }
}
