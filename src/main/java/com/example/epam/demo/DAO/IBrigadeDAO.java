package com.example.epam.demo.DAO;

import com.example.epam.demo.DTO.Brigade;

public interface IBrigadeDAO {
    Brigade addBrigade(Brigade brigade);

    Brigade getBrigadeById(long brigadeId);

    void deketeBrigade(long brigadeId);
}
