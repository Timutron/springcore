package com.example.epam.demo.DAO;

import com.example.epam.demo.DTO.Request;

public interface IRequestDAO {
    Request addRequest(Request request);

    Request getRequestById(long requestId);

    void deleteRequestById(long requestId);
}
