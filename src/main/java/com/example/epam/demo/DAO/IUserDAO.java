package com.example.epam.demo.DAO;

import com.example.epam.demo.DTO.User;

public interface IUserDAO {

    User addUser(User user);

    User getUserById(long userId);

    void deleteUser(long userId);


    User getUserByLoginAndPassword(String userLogin, String userPassword);
}
