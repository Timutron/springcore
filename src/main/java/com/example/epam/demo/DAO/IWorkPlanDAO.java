package com.example.epam.demo.DAO;

import com.example.epam.demo.DTO.WorkPlan;

import java.util.List;

public interface IWorkPlanDAO {

    WorkPlan createWorkPlan(WorkPlan workPlan);

     List<WorkPlan> getWorkPlan();

     int deleteWorkPlanById(long workPlanId);
}
