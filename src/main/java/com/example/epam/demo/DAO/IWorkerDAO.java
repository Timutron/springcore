package com.example.epam.demo.DAO;

import com.example.epam.demo.DTO.Worker;

public interface IWorkerDAO {

    Worker addWorker(Worker worker);

    Worker getWorkerById(long workerId);

    void deleteWorker(long workerId);
}
