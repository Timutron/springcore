package com.example.epam.demo.DAO;

import com.example.epam.demo.DTO.Request;
import com.example.epam.demo.DTO.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class RequesrDAO implements IRequestDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private RowMapper<Request> MyRowMapper = new BeanPropertyRowMapper<>(Request.class);

    @Override
    public Request addRequest(Request request) {
        String sql = "INSERT INTO request (requestName, brigadeId, workPlanId) VALUES (?, ?, ?)";
        jdbcTemplate.update(sql, request.getRequestName(), request.getBrigadeId(), request.getWorkPlanId());
        return request;
    }

    @Override
    public Request getRequestById(long requestId) {
        return null;
    }

    @Override
    public void deleteRequestById(long requestId) {

    }
}
