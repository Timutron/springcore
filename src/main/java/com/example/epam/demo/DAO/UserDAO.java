package com.example.epam.demo.DAO;

import com.example.epam.demo.DTO.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAO implements IUserDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private RowMapper<User> MyRowMapper = new BeanPropertyRowMapper<>(User.class);

    @Override
    public User addUser(User user) {
        String sql = "INSERT INTO user (userLogin, userPassword, userName," +
                " userSurname, userHouseNumber, userApartmentNumber, userStreetName) VALUES(?, ?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, user.getUserLogin(), user.getUserPassword(), user.getUserName(), user.getUserSurname(),
                user.getUserHouseNumber(), user.getUserApartmentNumber(), user.getUserStreetName());
        return user;
    }

    @Override
    public User getUserById(long userId) {
        String sql = "SELECT * FROM user WHERE userId = ?";
        return jdbcTemplate.queryForObject(sql, MyRowMapper, userId);
    }

    @Override
    public void deleteUser(long userId) {
        String sql = "DELETE * FROM user WHERE userId = ?";
        jdbcTemplate.queryForObject(sql, MyRowMapper, userId);
    }

    @Override
    public User getUserByLoginAndPassword(String userLogin, String userPassword) {
        String sql = "SELECT * FROM user WHERE userLogin = ? AND userPassword = ? ";
        return jdbcTemplate.queryForObject(sql, MyRowMapper, userLogin, userPassword);
    }
}
