package com.example.epam.demo.DAO;

import com.example.epam.demo.DTO.WorkPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class WorkPlanDAO implements IWorkPlanDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private RowMapper<WorkPlan> MyRowMapper = new BeanPropertyRowMapper<>(WorkPlan.class);

    public WorkPlan createWorkPlan(WorkPlan workPlan) {
        String sql = "INSERT INTO workPlan (phoneNumber, dateTime, problem, userId) VALUES (?, ?, ?, ?)";
        jdbcTemplate.update(sql, workPlan.getPhoneNumber(), workPlan.getDateTime(), workPlan.getProblem(), workPlan.getUserId());
        return workPlan;
    }

    @Override
    public List<WorkPlan> getWorkPlan() {
        String sql = "SELECT * FROM workPlan ORDER BY workPlanId";
        return jdbcTemplate.query(sql, MyRowMapper);
    }

    @Override
    public int deleteWorkPlanById(long workPlanId) {
        String sql = "DELETE  FROM workPlan WHERE workPlanId = ?";
        return jdbcTemplate.update(sql, workPlanId);
    }
}
