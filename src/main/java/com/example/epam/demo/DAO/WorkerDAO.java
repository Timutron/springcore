package com.example.epam.demo.DAO;

import com.example.epam.demo.DTO.Worker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class WorkerDAO implements IWorkerDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private RowMapper<Worker> MyRowMapper = new BeanPropertyRowMapper<>(Worker.class);

    @Override
    public Worker addWorker(Worker worker) {
        String sql = "INSERT INTO worker (profession, brigadeId) VALUES (?, ?)";
        jdbcTemplate.update(sql, worker.getProfession(), worker.getBrigadeId());
        return worker;
    }

    @Override
    public Worker getWorkerById(long workerId) {
        String sql = "SELECT FROM worker WHERE workerId = ?";
        return jdbcTemplate.queryForObject(sql, MyRowMapper, workerId);
    }

    @Override
    public void deleteWorker(long workerId) {
        String sql = "DELETE FROM worker WHERE workerId = ?";
        jdbcTemplate.queryForObject(sql, MyRowMapper, workerId);
    }
}
