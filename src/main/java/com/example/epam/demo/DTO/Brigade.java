package com.example.epam.demo.DTO;

public class Brigade {
    private long brigadeId;
    private String brigadeName;

    public Brigade() {
    }

    public Brigade(long brigadeId, String brigadeName) {
        this.brigadeId = brigadeId;
        this.brigadeName = brigadeName;
    }

    public long getBrigadeId() {
        return brigadeId;
    }

    public void setBrigadeId(long brigadeId) {
        this.brigadeId = brigadeId;
    }

    public String getBrigadeName() {
        return brigadeName;
    }

    public void setBrigadeName(String brigadeName) {
        this.brigadeName = brigadeName;
    }
}
