package com.example.epam.demo.DTO;

public class Request {
    private long requestId;
    private String requestName;
    private long brigadeId;
    private long workPlanId;

    public Request() {
    }

    public Request(long requestId, String requestName, long brigadeId, long workPlanId) {
        this.requestId = requestId;
        this.requestName = requestName;
        this.brigadeId = brigadeId;
        this.workPlanId = workPlanId;
    }

    public long getRequestId() {
        return requestId;
    }

    public void setRequestId(long requestId) {
        this.requestId = requestId;
    }

    public String getRequestName() {
        return requestName;
    }

    public void setRequestName(String requestName) {
        this.requestName = requestName;
    }

    public long getBrigadeId() {
        return brigadeId;
    }

    public void setBrigadeId(long brigadeId) {
        this.brigadeId = brigadeId;
    }

    public long getWorkPlanId() {
        return workPlanId;
    }

    public void setWorkPlanId(long workPlanId) {
        this.workPlanId = workPlanId;
    }
}