package com.example.epam.demo.DTO;

public class User {
    private long userId;
    private String userLogin;
    private String userPassword;
    private String userName;
    private String userSurname;
    private int userHouseNumber;
    private int userApartmentNumber;
    private String userStreetName;
    private String userRole;

    public User() {
    }

    public User(long userId, String userLogin, String userPassword, String userName, String userSurname,
                int userHouseNumber, int userApartmentNumber, String userStreetName, String userRole) {
        this.userId = userId;
        this.userLogin = userLogin;
        this.userPassword = userPassword;
        this.userName = userName;
        this.userSurname = userSurname;
        this.userHouseNumber = userHouseNumber;
        this.userApartmentNumber = userApartmentNumber;
        this.userStreetName = userStreetName;
        this.userRole = userRole;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserSurname() {
        return userSurname;
    }

    public void setUserSurname(String userSurname) {
        this.userSurname = userSurname;
    }

    public int getUserHouseNumber() {
        return userHouseNumber;
    }

    public void setUserHouseNumber(int userHouseNumber) {
        this.userHouseNumber = userHouseNumber;
    }

    public int getUserApartmentNumber() {
        return userApartmentNumber;
    }

    public void setUserApartmentNumber(int userApartmentNumber) {
        this.userApartmentNumber = userApartmentNumber;
    }

    public String getUserStreetName() {
        return userStreetName;
    }

    public void setUserStreetName(String userStreetName) {
        this.userStreetName = userStreetName;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }
}