package com.example.epam.demo.DTO;

public class WorkPlan {
    private long workPlanId;
    private String phoneNumber;
    private String dateTime;
    private String problem;
    private long userId;

    public WorkPlan() {
    }

    public WorkPlan(long workPlanId, String phoneNumber, String dateTime, String problem, long requestId, long userId) {
        this.workPlanId = workPlanId;
        this.phoneNumber = phoneNumber;
        this.dateTime = dateTime;
        this.problem = problem;
        this.userId = userId;
    }

    public long getWorkPlanId() {
        return workPlanId;
    }

    public void setWorkPlanId(long workPlanId) {
        this.workPlanId = workPlanId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Заявка №" + workPlanId +
                ", Телефон: " + phoneNumber +
                ", Дата и время: " + dateTime  +
                ", Описание проблемы: " + problem  +
                ", Заказчик: " + userId;
    }
}
