package com.example.epam.demo.DTO;

public class Worker {
    private long workerId;
    private String profession;
    private long brigadeId;

    public Worker() {
    }

    public Worker(long workerId, String profession, long brigadeId) {
        this.workerId = workerId;
        this.profession = profession;
        this.brigadeId = brigadeId;
    }

    public long getWorkerId() {
        return workerId;
    }

    public void setWorkerId(long workerId) {
        this.workerId = workerId;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public long getBrigadeId() {
        return brigadeId;
    }

    public void setBrigadeId(long brigadeId) {
        this.brigadeId = brigadeId;
    }
}
