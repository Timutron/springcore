package com.example.epam.demo.Service;

import com.example.epam.demo.DTO.Request;

public interface IRequestService {
    Request addRequest(Request request);
}
