package com.example.epam.demo.Service;

import com.example.epam.demo.DTO.User;

public interface IUserServise {
    User registrationUser(User user);

    User loginPassword(User user);
}
