package com.example.epam.demo.Service;

import com.example.epam.demo.DTO.WorkPlan;

import java.util.List;

public interface IWorkPlanService {

    WorkPlan addWorkPlan(WorkPlan workPlan);

    List<WorkPlan> takeWorkPlan();

    int deleteWorkPlan(long workPlanId);
}