package com.example.epam.demo.Service;

import com.example.epam.demo.DAO.IRequestDAO;
import com.example.epam.demo.DTO.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class RequestService implements IRequestService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    IRequestDAO iRequestDAO;

    @Override
    public Request addRequest(Request request) {
        return iRequestDAO.addRequest(request);
    }
}
