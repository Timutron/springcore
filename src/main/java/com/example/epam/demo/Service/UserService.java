package com.example.epam.demo.Service;

import com.example.epam.demo.DAO.IUserDAO;
import com.example.epam.demo.DTO.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService implements IUserServise {

    @Autowired
    IUserDAO iUserDAO;

    @Override
    public User registrationUser(User user) {
        return iUserDAO.addUser(user);
    }

    public User loginPassword(User user) {
        return iUserDAO.getUserByLoginAndPassword(user.getUserLogin(), user.getUserPassword());
    }
}
