package com.example.epam.demo.Service;

import com.example.epam.demo.DAO.IWorkPlanDAO;
import com.example.epam.demo.DTO.WorkPlan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorkPlanService implements IWorkPlanService {

    @Autowired
    IWorkPlanDAO iWorkPlanDAO;

    public WorkPlan addWorkPlan(WorkPlan workPlan){
        return iWorkPlanDAO.createWorkPlan(workPlan);
    }

    @Override
    public List<WorkPlan> takeWorkPlan() {
        return iWorkPlanDAO.getWorkPlan();
    }

    @Override
    public int deleteWorkPlan(long workPlanId) {
        return iWorkPlanDAO.deleteWorkPlanById(workPlanId);
    }
}
