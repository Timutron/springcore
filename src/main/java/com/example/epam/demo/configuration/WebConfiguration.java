package com.example.epam.demo.configuration;

import com.example.epam.demo.interseptor.AuthInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    @Bean
    public AuthInterceptor authInterceptor() {
        return new AuthInterceptor();
    }

//    @Bean
//    public UserNameAwareInterceptor userNameAwareInterceptor() {
//        return new UserNameAwareInterceptor();
//    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authInterceptor()).addPathPatterns("/**")
                .excludePathPatterns("/home", "/error", "/login", "/registration", "/logout", "/image/**", "/css/**", "/js/**");

//        registry.addInterceptor(userNameAwareInterceptor()).addPathPatterns("/**")
//                .excludePathPatterns("/login",
//                        "/registration", "/logout", "/css/**", "/js/", "/home");
    }
}
