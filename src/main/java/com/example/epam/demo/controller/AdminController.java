package com.example.epam.demo.controller;

import com.example.epam.demo.DTO.Request;
import com.example.epam.demo.Service.IRequestService;
import com.example.epam.demo.Service.IWorkPlanService;
import com.example.epam.demo.Service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AdminController {

    @Autowired
    IWorkPlanService iWorkPlanService;

    @Autowired
    IRequestService iRequestService;

    @Autowired
    UserManager userManager;

    @GetMapping ("/adminPage")
    public ModelAndView takeWorkPlan(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("workPlan", iWorkPlanService.takeWorkPlan());
        modelAndView.addObject(userManager.getUser());
        modelAndView.setViewName("adminPage");
        return modelAndView;
    }

    @PostMapping("/adminPage")
    public ModelAndView addRequerst(Request request){
        ModelAndView modelAndView = new ModelAndView();
        iRequestService.addRequest(request);
        modelAndView.setViewName("adminPage");
        return modelAndView;
    }

}
