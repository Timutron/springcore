package com.example.epam.demo.controller;

import com.example.epam.demo.DTO.User;
import com.example.epam.demo.Service.UserManager;
import com.example.epam.demo.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class EnterController {

    @Autowired
    UserManager userManager;

    @Autowired
    UserService userService;

    @GetMapping("/login")
    public ModelAndView loginPage(){
        ModelAndView model = new ModelAndView();
        model.setViewName("login");
        return model;
    }

    @PostMapping ("/login")
    public ModelAndView authUser (User user) {
        ModelAndView modelAndView = new ModelAndView();

        try {
            User currentUser = userService.loginPassword(user);
            userManager.setUser(currentUser);
        }catch (EmptyResultDataAccessException ex){
            modelAndView.addObject("noUser", "Такой пользователь не найден");
            modelAndView.setViewName("login");
            modelAndView.addObject("userLogin", user.getUserLogin());
            return modelAndView;
        }

        modelAndView.setViewName("redirect:/userPage");
        return modelAndView;
    }

    @GetMapping ("/userPage")
    public ModelAndView userPage() {
        ModelAndView modelAndView = new ModelAndView();

        User currentUser = userManager.getUser();

        if (currentUser.getUserRole().equals("ADMIN")){
            modelAndView.setViewName("adminPage");
        } else {
            modelAndView.setViewName("userPage");
        }
        modelAndView.addObject(currentUser);

        return modelAndView;
    }

    @GetMapping("/logout")
    public ModelAndView logout(HttpServletRequest request) {
        ModelAndView model = new ModelAndView();

        request.getSession().invalidate();
        model.setViewName("redirect:/home");

        return model;
    }

}
