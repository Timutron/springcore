package com.example.epam.demo.controller;

import com.example.epam.demo.DTO.User;
import com.example.epam.demo.Service.IUserServise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RegistrationController {

    @Autowired
    IUserServise iUserServise;

    @GetMapping("/registration")
    public ModelAndView regPage(){
        ModelAndView model = new ModelAndView();
        model.setViewName("registration");
        return model;
    }


    @PostMapping("/registration")
    public ModelAndView registrationUser(User user){
        ModelAndView modelAndView = new ModelAndView();
        iUserServise.registrationUser(user);
        modelAndView.setViewName("userPage");
        modelAndView.addObject(user);
        return modelAndView;
    }
}
