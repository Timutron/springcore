package com.example.epam.demo.controller;

import com.example.epam.demo.DAO.IWorkPlanDAO;
import com.example.epam.demo.DTO.WorkPlan;
import com.example.epam.demo.Service.IWorkPlanService;
import com.example.epam.demo.Service.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserPageController {

    @Autowired
    IWorkPlanService iWorkPlanService;

    @Autowired
    UserManager userManager;

    @GetMapping("/success")
    public ModelAndView SuccesGet(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("home");
        return modelAndView;
    }

    @PostMapping("/success")
    public ModelAndView Success(WorkPlan workPlan){
        ModelAndView modelAndView = new ModelAndView();
        workPlan.setUserId(userManager.getUser().getUserId());
        iWorkPlanService.addWorkPlan(workPlan);
        modelAndView.setViewName("success");
        return modelAndView;
    }
}
