package com.example.epam.demo.exeptions;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class GlobalExeptionHandler {

    @ExceptionHandler(Exception.class)
    public ModelAndView HandleExeption(Exception ex){
        ModelAndView modelAndView = new ModelAndView();
        ex.printStackTrace();
        modelAndView.setViewName("error");
        return modelAndView;
    }
}
