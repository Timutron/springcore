 <%@ page contentType="text/html; charset=utf-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Администратор</title>
        <link rel="stylesheet" href="../css/adminPage.css" type="text/css">
    </head>
    <body>
        <h1>Пришло время разгребать ... ${user.userName}</h1>
        <div class="workPlan">
            <c:forEach var="work" items="${workPlan}">
                <p>${work}</p>
            </c:forEach>
        </div>
        <div class="request">
            <p>Оформите заявку</p>
            <form action="/adminPage" method="POST">
                <input type="text" name="requestName" placeholder="Название заявки">
                <input type="text" name="brigadeId" placeholder="Номер бригады">
                <input type="text" name="workPlanId" placeholder="Номер заявки">
                <input type="submit" id="subInDiv" value="Создать">
            </form>
        </div>
        <form action="/adminPage" method="get">
            <input type="submit"  value="Обновить">
        </form>
    </body>
</html>