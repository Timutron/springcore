<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <title>
            ОШИБКА
        </title>
        <link rel="stylesheet" href="../css/error.css" type="text/css">
    </head>
    <body>
        <h1>
            Что-то пошло не так, разработчик только учится:) пожалуйста, никому не сообщайте:)
        </h1>
    </body>
</html>