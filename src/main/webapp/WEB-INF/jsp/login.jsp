 <%@ page contentType="text/html; charset=utf-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<head>
    <title>Страница входа</title>
    <link rel="stylesheet" href="../css/Reg.css" type="text/css">
</head>
<body>
        <div class="forma">
        <p>${noUser}</p>
            <form action="/login" method="post" id="login-form">
                <input type="text" id="lgn" name="userLogin" placeholder="Введите логин" value = "${userLogin}">
                <input type="password" id="psw" name="userPassword" placeholder="Введите пароль">
                <input type="submit" value="Вход">
            </form>
        </div>
</body>
</html>