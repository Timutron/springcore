 <%@ page contentType="text/html; charset=utf-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<head>
    <title>Страница входа</title>
    <link rel="stylesheet" href="../css/Reg.css" type="text/css">
</head>
<body>
        <div class="forma">
            <form action="/registration" method="post" id="registration-form">
                <input type="text" id="lgn" name="userLogin" placeholder="Придумайте логин">
                <input type="password" id="psw" name="userPassword" placeholder="Придумайте пароль">
                <input type="text" id="name" name="userName" placeholder="Ваше имя">
                <input type="text" id="surname" name="userSurname" placeholder="Ваша фамилия">
                <input type="text" id="houseNumber" name="userHouseNumber" placeholder="Введите номер дома">
                <input type="text" id="apartmentNumber" name="userApartmentNumber" placeholder="Введите номер квартиры">
                <input type="text" id="street" name="userStreetName" placeholder="Введите название улицы">
                <input type="submit" value="Регистрация">
            </form>
        </div>
</body>
</html>