<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <title>
            Успешно
        </title>
        <link rel="stylesheet" href="../css/success.css" type="text/css">
    </head>
    <body>
        <h1>
            Ваш запрос успешно отправлен!
        </h1>
        <form action="/logout" method="GET">
            <input type="submit" value="Выход">
        </form>
    </body>
</html>