<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Страница пользователя</title>
        <link rel="stylesheet" href="../css/userPageForm.css" type="text/css">
    </head>
    <body>
        <h1>Привет, ${user.userName}</h1>
        <h2>Заполните заявку</h2>
        <div class="form">
            <form action="/success" method="post" id="user-form">
                <input type="text" id="NF" name="phoneNumber" placeholder="Укажите Ваш номер телефона">
                <input type="text" id="DT" name="dateTime" placeholder="Введите дату и время в формате (дд.мм.гг. время)">
                <textarea id="problem" name="problem" placeholder="Опишите вкратце Вашу проблему"></textarea>
                <input type="submit" value="Отправить">
            </form>
        </div>
        <p>Мы вам обязательно поможем</p>
    </body>
</html>